﻿<SyntaxDefinition name="HLSL"
        xmlns="http://icsharpcode.net/sharpdevelop/syntaxdefinition/2008">

  <Color name="Comment" foreground="LimeGreen" />
  <Color name="Type" foreground="RoyalBlue" />
  <Color name="ControlInstr" foreground="DodgerBlue" />
  <Color name="Number" foreground="Pink" />
  <Color name="BuiltInVars" foreground="DarkOrange" />
  <Color name="BuiltInConst" foreground="Orange" />
  <Color name="Preprocessor" foreground="LightSteelBlue" />
  <RuleSet ignoreCase="false">

    <!-- Comments -->
    <Span color="Comment" begin="//" />
    <Span color="Comment" multiline="true"
       begin="/\*" end="\*/" />

    <!-- Preprocessor -->
    <Span color="Preprocessor" begin="#" />

    <!-- Builtin types -->
    <Keywords color="Type" fontWeight="bold">
      <Word>Buffer</Word>
      <Word>sampler</Word>
      <Word>SamplerState</Word>
      <Word>texture</Word>
      <Word>Texture1D</Word>
      <Word>Texture2D</Word>
      <Word>Texture3D</Word>
      <Word>Texture1DArray</Word>
      <Word>Texture2DArray</Word>
      <Word>TextureCube</Word>
      <Word>bool</Word>
      <Word>int</Word>
      <Word>uint</Word>
      <Word>dword</Word>
      <Word>half</Word>
      <Word>float</Word>
      <Word>double</Word>
      <Word>bool1</Word>
      <Word>bool2</Word>
      <Word>bool3</Word>
      <Word>bool4</Word>
      <Word>bool1x1</Word>
      <Word>bool1x2</Word>
      <Word>bool1x3</Word>
      <Word>bool1x4</Word>
      <Word>bool2x1</Word>
      <Word>bool2x2</Word>
      <Word>bool2x3</Word>
      <Word>bool2x4</Word>
      <Word>bool3x1</Word>
      <Word>bool3x2</Word>
      <Word>bool3x3</Word>
      <Word>bool3x4</Word>
      <Word>bool4x1</Word>
      <Word>bool4x2</Word>
      <Word>bool4x3</Word>
      <Word>bool4x4</Word>
      <Word>int1</Word>
      <Word>int2</Word>
      <Word>int3</Word>
      <Word>int4</Word>
      <Word>int1x1</Word>
      <Word>int1x2</Word>
      <Word>int1x3</Word>
      <Word>int1x4</Word>
      <Word>int2x1</Word>
      <Word>int2x2</Word>
      <Word>int2x3</Word>
      <Word>int2x4</Word>
      <Word>int3x1</Word>
      <Word>int3x2</Word>
      <Word>int3x3</Word>
      <Word>int3x4</Word>
      <Word>int4x1</Word>
      <Word>int4x2</Word>
      <Word>int4x3</Word>
      <Word>int4x4</Word>
      <Word>uint1</Word>
      <Word>uint2</Word>
      <Word>uint3</Word>
      <Word>uint4</Word>
      <Word>uint1x1</Word>
      <Word>uint1x2</Word>
      <Word>uint1x3</Word>
      <Word>uint1x4</Word>
      <Word>uint2x1</Word>
      <Word>uint2x2</Word>
      <Word>uint2x3</Word>
      <Word>uint2x4</Word>
      <Word>uint3x1</Word>
      <Word>uint3x2</Word>
      <Word>uint3x3</Word>
      <Word>uint3x4</Word>
      <Word>uint4x1</Word>
      <Word>uint4x2</Word>
      <Word>uint4x3</Word>
      <Word>uint4x4</Word>
      <Word>dword1</Word>
      <Word>dword2</Word>
      <Word>dword3</Word>
      <Word>dword4</Word>
      <Word>dword1x1</Word>
      <Word>dword1x2</Word>
      <Word>dword1x3</Word>
      <Word>dword1x4</Word>
      <Word>dword2x1</Word>
      <Word>dword2x2</Word>
      <Word>dword2x3</Word>
      <Word>dword2x4</Word>
      <Word>dword3x1</Word>
      <Word>dword3x2</Word>
      <Word>dword3x3</Word>
      <Word>dword3x4</Word>
      <Word>dword4x1</Word>
      <Word>dword4x2</Word>
      <Word>dword4x3</Word>
      <Word>dword4x4</Word>
      <Word>half1</Word>
      <Word>half2</Word>
      <Word>half3</Word>
      <Word>half4</Word>
      <Word>half1x1</Word>
      <Word>half1x2</Word>
      <Word>half1x3</Word>
      <Word>half1x4</Word>
      <Word>half2x1</Word>
      <Word>half2x2</Word>
      <Word>half2x3</Word>
      <Word>half2x4</Word>
      <Word>half3x1</Word>
      <Word>half3x2</Word>
      <Word>half3x3</Word>
      <Word>half3x4</Word>
      <Word>half4x1</Word>
      <Word>half4x2</Word>
      <Word>half4x3</Word>
      <Word>half4x4</Word>
      <Word>float1</Word>
      <Word>float2</Word>
      <Word>float3</Word>
      <Word>float4</Word>
      <Word>float1x1</Word>
      <Word>float1x2</Word>
      <Word>float1x3</Word>
      <Word>float1x4</Word>
      <Word>float2x1</Word>
      <Word>float2x2</Word>
      <Word>float2x3</Word>
      <Word>float2x4</Word>
      <Word>float3x1</Word>
      <Word>float3x2</Word>
      <Word>float3x3</Word>
      <Word>float3x4</Word>
      <Word>float4x1</Word>
      <Word>float4x2</Word>
      <Word>float4x3</Word>
      <Word>float4x4</Word>
      <Word>double1</Word>
      <Word>double2</Word>
      <Word>double3</Word>
      <Word>double4</Word>
      <Word>double1x1</Word>
      <Word>double1x2</Word>
      <Word>double1x3</Word>
      <Word>double1x4</Word>
      <Word>double2x1</Word>
      <Word>double2x2</Word>
      <Word>double2x3</Word>
      <Word>double2x4</Word>
      <Word>double3x1</Word>
      <Word>double3x2</Word>
      <Word>double3x3</Word>
      <Word>double3x4</Word>
      <Word>double4x1</Word>
      <Word>double4x2</Word>
      <Word>double4x3</Word>
      <Word>double4x4</Word>
      <Word>matrix</Word>


    </Keywords>

    <!-- Numbers -->
    <Rule color="Number">
      \-[0-9] #int (neg)
      | (	\b[+-]?\d+(\.[0-9]+[f]?)? |	[+-]?\.[0-9]+[f]?	) #float
    </Rule>

    <!-- Control instructions -->
    <Keywords color="ControlInstr" fontWeight="bold">
      <Word>struct</Word>
      <Word>const</Word>


      <Word>for</Word>
      <Word>while</Word>
      <Word>do</Word>
      <Word>break</Word>
      <Word>continue</Word>
      <Word>if</Word>
      <Word>else</Word>
      <Word>switch</Word>
      <Word>case</Word>
      <Word>default</Word>
      <Word>return</Word>
      <Word>discard</Word>
      <Word>typedef</Word>
      <Word>register</Word>
      <Word>packoffset</Word>
      <Word>cbuffer</Word>
      <Word>tbuffer</Word>

    </Keywords>
  
    <Keywords color="BuiltInVars" fontWeight="bold">
      <Word>SV_ClipDistance</Word>
      <Word>SV_CullDistance</Word>
      <Word>SV_Coverage</Word>
      <Word>SV_Depth</Word>
      <Word>SV_DepthGreaterEqual</Word>
      <Word>SV_DepthLessEqual</Word>
      <Word>SV_DispatchThreadID</Word>
      <Word>SV_DomainLocation</Word>
      <Word>SV_GroupID</Word>
      <Word>SV_GroupIndex</Word>
      <Word>SV_GroupThreadID</Word>
      <Word>SV_GSInstanceID</Word>
      <Word>SV_InnerCoverage</Word>
      <Word>SV_InsideTessFactor</Word>
      <Word>SV_InstanceID</Word>
      <Word>SV_IsFrontFace</Word>
      <Word>SV_OutputControlPointID</Word>
      <Word>SV_Position</Word>
      <Word>SV_PrimitiveID</Word>
      <Word>SV_RenderTargetArrayIndex</Word>
      <Word>SV_SampleIndex</Word>
      <Word>SV_StencilRef</Word>
      <Word>SV_Target</Word>
      <Word>SV_TessFactor</Word>
      <Word>SV_VertexID</Word>
      <Word>SV_ViewportArrayIndex</Word>
    
    </Keywords>
    
    <Keywords color="BuiltInConst" fontWeight="bold">
      <Word>BINORMAL</Word>
      <Word>BLENDINDICES</Word>
      <Word>BLENDWEIGHT</Word>
      <Word>COLOR</Word>
      <Word>NORMAL</Word>
      <Word>POSITION</Word>
      <Word>POSITIONT</Word>
      <Word>PSIZE</Word>
      <Word>TANGENT</Word>
      <Word>TEXCOORD</Word>
      <Word>FOG</Word>
      <Word>TESSFACTOR</Word>
      <Word>VFACE</Word>
      <Word>VPOS</Word>
      <Word>DEPTH</Word>
    </Keywords>
</RuleSet>
  

</SyntaxDefinition>