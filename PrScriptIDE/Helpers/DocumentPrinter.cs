﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace PrScriptIDE.Helpers
{
    public static class DocumentPrinter
    {
        public static FlowDocument CreateFlowDocumentForEditor(TextEditor editor, string path, bool highl)
        {
            IHighlighter highlighter;
            if (highl)
                highlighter = editor.TextArea.GetService(typeof(IHighlighter)) as IHighlighter;
            else
                highlighter = null;

            Paragraph paraHeader = new Paragraph();
            paraHeader.FontSize = 10;
            paraHeader.Foreground = new SolidColorBrush(Colors.Black);
            paraHeader.FontWeight = FontWeights.Bold;
            paraHeader.KeepTogether = true;
            TextBlock blc = new TextBlock();
            blc.Text = "PrScriptIDE  File:" + path;
            blc.TextWrapping = TextWrapping.NoWrap;

            paraHeader.Inlines.Add(blc);
            paraHeader.Inlines.Add(new Run(""));

            FlowDocument doc = new FlowDocument(paraHeader);

            doc.Blocks.Add(ICSharpCode.AvalonEdit.Utils.DocumentPrinter.ConvertTextDocumentToBlock(editor.Document, highlighter));
            doc.FontFamily = editor.FontFamily;
            doc.FontSize = editor.FontSize;
            return doc;
        }
    }
}