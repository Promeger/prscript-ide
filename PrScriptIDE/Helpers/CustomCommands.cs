﻿using System.Windows.Input;

namespace PrScriptIDE.Helpers
{
    public static class CustomCommands
    {
        public static readonly RoutedUICommand Exit = new RoutedUICommand
                        (
                                "Exit",
                                "Exit",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F4, ModifierKeys.Alt)
                                }
                        );

        public static readonly RoutedUICommand FormatCode = new RoutedUICommand
                       (
                               "FormatCode",
                               "FormatCode",
                               typeof(CustomCommands),
                               new InputGestureCollection()
                               {
                                        new KeyGesture(Key.F8, ModifierKeys.Shift)
                               }
                       );

        public static readonly RoutedUICommand ApplyFolding = new RoutedUICommand
                       (
                               "ApplyFolding",
                               "ApplyFolding",
                               typeof(CustomCommands),
                               new InputGestureCollection()
                               {
                                        new KeyGesture(Key.F10, ModifierKeys.Shift)
                               }
                       );
    }
}