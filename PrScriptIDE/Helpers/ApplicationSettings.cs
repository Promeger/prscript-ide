﻿using System;
using System.IO;
using System.Windows;

namespace PrScriptIDE.Helpers
{
    public class ApplicationSettings
    {
        private string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PrEngine\\PrScriptIDE\\settings.dat";

        public bool foldingEnabled { get; set; }
        public bool foldingAuto { get; set; }
        public bool formatEnabled { get; set; }
        public bool formatAuto { get; set; }
        public bool printHighl { get; set; }
        public bool completion { get; set; }

        public bool exist()
        {
            if (File.Exists(path))
                return true;
            else
                return false;
        }

        public void create()
        {
            if (!Directory.Exists(path.Substring(0, path.LastIndexOf('\\'))))
            {
                Directory.CreateDirectory(path.Substring(0, path.LastIndexOf('\\')));
            }

            using (StreamWriter sw = File.CreateText(path))
            {
                sw.WriteLine("PrScriptIDE Settings File");
                sw.WriteLine("foldingEnabled=true;");
                sw.WriteLine("foldingAuto=true;");
                sw.WriteLine("formatEnabled=true;");
                sw.WriteLine("formatAuto=true;");
                sw.WriteLine("printHighlighting=true;");
                sw.WriteLine("completion=true;");
                sw.Close();
            }
        }

        public void read()
        {
            using (StreamReader sr = File.OpenText(path))
            {
                sr.ReadLine();
                string line = sr.ReadLine();

                try
                {
                    foldingEnabled = bool.Parse(line.Substring(line.IndexOf('=') + 1, line.IndexOf(';') - line.IndexOf('=') - 1));
                    line = sr.ReadLine();
                    foldingAuto = bool.Parse(line.Substring(line.IndexOf('=') + 1, line.IndexOf(';') - line.IndexOf('=') - 1));
                    line = sr.ReadLine();
                    formatEnabled = bool.Parse(line.Substring(line.IndexOf('=') + 1, line.IndexOf(';') - line.IndexOf('=') - 1));
                    line = sr.ReadLine();
                    formatAuto = bool.Parse(line.Substring(line.IndexOf('=') + 1, line.IndexOf(';') - line.IndexOf('=') - 1));
                    line = sr.ReadLine();
                    printHighl = bool.Parse(line.Substring(line.IndexOf('=') + 1, line.IndexOf(';') - line.IndexOf('=') - 1));
                    line = sr.ReadLine();
                    completion = bool.Parse(line.Substring(line.IndexOf('=') + 1, line.IndexOf(';') - line.IndexOf('=') - 1));
                    sr.Close();
                }
                catch
                {
                    MessageBox.Show("Settings file contains unauthorized changes. Resets settings to default!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    sr.Close();
                    create();
                }
            }
        }

        public void save()
        {
            using (StreamWriter sw = File.CreateText(path))
            {
                sw.WriteLine("PrScriptIDE Settings File");
                sw.WriteLine(foldingEnabled ? "foldingEnabled=true;" : "foldingEnabled=false;");
                sw.WriteLine(foldingAuto ? "foldingAuto=true;" : "foldingAuto=false;");
                sw.WriteLine(formatEnabled ? "formatEnabled=true;" : "formatEnabled=false;");
                sw.WriteLine(formatAuto ? "formatAuto=true;" : "formatAuto=false;");
                sw.WriteLine(printHighl ? "printHighlighting=true;" : "printHighlighting=false;");
                sw.WriteLine(completion ? "completion=true;" : "completion=false;");
                sw.Close();
            }
        }
    }
}