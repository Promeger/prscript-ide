﻿using MahApps.Metro.Controls;
using System.Windows;

namespace PrScriptIDE.Components
{
    /// <summary>
    /// Interaction logic for PreferencesWindow.xaml
    /// </summary>
    public partial class PreferencesWindow : MetroWindow
    {
        private MainWindow owner;
        public bool folAuto { get; set; }
        public bool folEn { get; set; }
        public bool forAuto { get; set; }
        public bool forEn { get; set; }
        public bool printHighl { get; set; }
        public bool completion { get; set; }

        public PreferencesWindow(MainWindow own)
        {
            InitializeComponent();

            owner = own;
            this.DataContext = this;
            this.folAuto = own.m_settings.foldingAuto;
            this.folEn = own.m_settings.foldingEnabled;
            this.forAuto = own.m_settings.formatAuto;
            this.forEn = own.m_settings.formatEnabled;
            this.printHighl = own.m_settings.printHighl;
            this.completion = own.m_settings.completion;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            owner.m_settings.save();
            this.Close();
        }

        private void CheckBoxChanged(object sender, RoutedEventArgs e)
        {
            owner.m_settings.foldingAuto = folAuto;
            owner.m_settings.foldingEnabled = folEn;
            owner.m_settings.formatAuto = forAuto;
            owner.m_settings.formatEnabled = forEn;
            owner.m_settings.printHighl = printHighl;
            owner.m_settings.completion = completion;
        }
    }
}