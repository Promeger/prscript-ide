﻿using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Resources;
using System.Xml;

namespace PrScriptIDE.Components
{
    public class Document
    {
        public TabItem m_tab;
        public TextBoxEx m_textBox;
        private string m_header;
        public string m_path = "";
        public bool m_saved;
        public MainWindow wnd;
        public int hlMode = 0;

        public Document(MainWindow win)
        {
            m_tab = new TabItem();

            m_tab.Header = m_header = "untitled";
            m_textBox = new TextBoxEx(win, this);
            m_tab.Content = m_textBox;
            m_saved = false;
            int val = m_tab.CommandBindings.Count;
            m_saved = true;
            wnd = win;
            hlMode = 0;

            setHighlighting("");
        }

        private string getExtension(string path)
        {
            return path.Substring(path.LastIndexOf('.') + 1);
        }

        private string getFileName(string path)
        {
            return path.Substring(path.LastIndexOf('\\') + 1);
        }

        public void setHighlighting(string syntax)
        {
            if (syntax.ToLower() == "prscript")
            {
                wnd.hl_none.IsChecked = false;
                wnd.hl_glsl.IsChecked = false;
                wnd.hl_hlsl.IsChecked = false;
                wnd.hl_prs.IsChecked = true;
                wnd.hl_pra.IsChecked = false;
                Uri uri = new Uri("Resources/PrScriptHighl.xshd", UriKind.Relative);
                StreamResourceInfo info = Application.GetResourceStream(uri);
                XmlReader reader = XmlReader.Create(info.Stream);
                m_textBox.editor.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                hlMode = 1;
            }
            else if (syntax.ToLower() == "prasm")
            {
                wnd.hl_none.IsChecked = false;
                wnd.hl_glsl.IsChecked = false;
                wnd.hl_hlsl.IsChecked = false;
                wnd.hl_prs.IsChecked = false;
                wnd.hl_pra.IsChecked = true;

                Uri uri = new Uri("Resources/PrASM.xshd", UriKind.Relative);
                StreamResourceInfo info = Application.GetResourceStream(uri);
                XmlReader reader = XmlReader.Create(info.Stream);
                m_textBox.editor.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                hlMode = 4;
            }
            else if (syntax.ToLower() == "glsl")
            {
                wnd.hl_none.IsChecked = false;
                wnd.hl_glsl.IsChecked = true;
                wnd.hl_hlsl.IsChecked = false;
                wnd.hl_prs.IsChecked = false;
                wnd.hl_pra.IsChecked = false;

                Uri uri = new Uri("Resources/GLSL.xshd", UriKind.Relative);
                StreamResourceInfo info = Application.GetResourceStream(uri);
                XmlReader reader = XmlReader.Create(info.Stream);
                m_textBox.editor.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                hlMode = 2;
            }
            else if (syntax.ToLower() == "hlsl")
            {
                wnd.hl_none.IsChecked = false;
                wnd.hl_glsl.IsChecked = false;
                wnd.hl_hlsl.IsChecked = true;
                wnd.hl_prs.IsChecked = false;
                wnd.hl_pra.IsChecked = false;

                Uri uri = new Uri("Resources/HLSL.xshd", UriKind.Relative);
                StreamResourceInfo info = Application.GetResourceStream(uri);
                XmlReader reader = XmlReader.Create(info.Stream);
                m_textBox.editor.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                hlMode = 3;
            }
            else
            {
                wnd.hl_none.IsChecked = true;
                wnd.hl_glsl.IsChecked = false;
                wnd.hl_hlsl.IsChecked = false;
                wnd.hl_prs.IsChecked = false;
                wnd.hl_pra.IsChecked = false;

                m_textBox.editor.SyntaxHighlighting = null;
                hlMode = 0;
            }
        }

        public void readFileAndDetermineSyntax(string path)
        {
            m_path = path;

            setHighlighting(getExtension(path));

            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    String line = sr.ReadToEnd();
                    m_textBox.editor.Text = line;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Couldn't read this file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            m_tab.Header = m_header = getFileName(path);
        }

        public bool isSaved()
        {
            return m_saved;
        }

        public void processSaveColor()
        {
            m_tab.ClearValue(TabItem.BackgroundProperty);

            wnd.stateText.Text = "Ready";
        }

        public void processSave()
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(m_path))
                {
                    sw.Write(m_textBox.editor.Text);
                    sw.Close();
                }

                readFileAndDetermineSyntax(m_path);
            }
            catch (Exception)
            {
                MessageBox.Show("Couldn't save this file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            processSaveColor();
        }
    }
}