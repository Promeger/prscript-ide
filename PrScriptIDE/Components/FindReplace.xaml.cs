﻿using ICSharpCode.AvalonEdit.Document;
using MahApps.Metro.Controls;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace PrScriptIDE.Components
{
    /// <summary>
    /// Interaction logic for FindReplace.xaml
    /// </summary>
    public partial class FindReplace : MetroWindow
    {
        public bool findRegex { get; set; }

        public TextBoxEx owntextBox;

        public FindReplace(TextBoxEx tb, bool replace = false)
        {
            InitializeComponent();
            owntextBox = tb;
            this.DataContext = this;

            if (replace)
                tabControl.SelectedIndex = 1;
        }

        private bool findNext(bool replace)
        {
            Regex regex = GetRegEx((replace ? find_rep : find_fnd).Text, (replace ? true : false));
            int start = regex.Options.HasFlag(RegexOptions.RightToLeft) ?
            owntextBox.editor.SelectionStart : owntextBox.editor.SelectionStart + owntextBox.editor.SelectionLength;
            Match match = regex.Match(owntextBox.editor.Text, start);

            if (!match.Success)
            {
                if (regex.Options.HasFlag(RegexOptions.RightToLeft))
                    match = regex.Match(owntextBox.editor.Text, owntextBox.editor.Text.Length);
                else
                    match = regex.Match(owntextBox.editor.Text, 0);
            }

            if (match.Success)
            {
                owntextBox.editor.Select(match.Index, match.Length);
                TextLocation loc = owntextBox.editor.Document.GetLocation(match.Index);
                owntextBox.editor.ScrollTo(loc.Line, loc.Column);
            }

            return match.Success;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (!findNext(true))
                MessageBox.Show(this, "Search function reached the end of the document", "End of the document", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void MetroWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.Close();
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                if (!findNext(false))
                    MessageBox.Show(this, "Search function reached the end of the document", "End of the document", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void textBox_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                if (!findNext(false))
                    MessageBox.Show(this, "Search function reached the end of the document", "End of the document", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!findNext(false))
                MessageBox.Show(this, "Search function reached the end of the document", "End of the document", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private Regex GetRegEx(string textToFind, bool replace, bool leftToRight = false)
        {
            RegexOptions options = RegexOptions.None;
            if ((replace ? up_rep : up_fnd).IsChecked == true && !leftToRight)
                options |= RegexOptions.RightToLeft;
            if ((replace ? case_rep : case_fnd).IsChecked == false)
                options |= RegexOptions.IgnoreCase;

            if ((replace ? regex_rep : regex_fnd).IsChecked == true)
            {
                return new Regex(textToFind, options);
            }
            else
            {
                string pattern = Regex.Escape(textToFind);
                if ((replace ? wild_rep : wild_fnd).IsChecked == true)
                    pattern = pattern.Replace("\\*", ".*").Replace("\\?", ".");
                if ((replace ? whole_rep : whole_fnd).IsChecked == true)
                    pattern = "\\b" + pattern + "\\b";
                return new Regex(pattern, options);
            }
        }

        private void replb_rep_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = GetRegEx(find_rep.Text, true);
            string input = owntextBox.editor.Text.Substring(owntextBox.editor.SelectionStart, owntextBox.editor.SelectionLength);
            Match match = regex.Match(input);
            bool replaced = false;
            if (match.Success && match.Index == 0 && match.Length == input.Length)
            {
                owntextBox.editor.Document.Replace(owntextBox.editor.SelectionStart, owntextBox.editor.SelectionLength, repl_rep.Text);
                replaced = true;
            }

            if (!findNext(true) && !replaced)
                MessageBox.Show(this, "Search function reached the end of the document", "End of the document", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void replallb_repl_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = GetRegEx(find_rep.Text, true, true);
            int offset = 0;
            owntextBox.editor.BeginChange();
            foreach (Match match in regex.Matches(owntextBox.editor.Text))
            {
                owntextBox.editor.Document.Replace(offset + match.Index, match.Length, repl_rep.Text);
                offset += repl_rep.Text.Length - match.Length;
            }

            owntextBox.editor.EndChange();
        }
    }
}