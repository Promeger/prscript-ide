﻿using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit.Indentation.CSharp;
using PrScriptIDE.Helpers;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace PrScriptIDE.Components
{
    /// <summary>
    /// Interaction logic for TextBox.xaml
    /// </summary>
    public partial class TextBoxEx : UserControl
    {
        private FoldingManager foldingManager;
        private MainWindow own;
        private Document docO;
        private BraceFoldingStrategy foldingStrategy;
        private CSharpIndentationStrategy indent;
        private CompletionWindow completion;
        private IList<ICompletionData> complData;
        private int spaceCnt;

        public TextBoxEx(MainWindow owner, Document doc)
        {
            InitializeComponent();
            this.own = owner;
            this.docO = doc;
            own.colCntr.Text = "Col " + getColumn().ToString();
            own.lineCntr.Text = "Ln " + getLine().ToString();
            foldingManager = FoldingManager.Install(this.editor.TextArea);
            foldingStrategy = new BraceFoldingStrategy();
            indent = new CSharpIndentationStrategy();
            editor.TextArea.TextEntering += textEditor_TextArea_TextEntering;
            editor.TextArea.TextEntered += textEditor_TextArea_TextEntered;
            editor.TextArea.KeyDown += TextArea_KeyDown;

            spaceCnt = 0;
        }

        private void CompletionList_InsertionRequested(object sender, System.EventArgs e)
        {
            spaceCnt = 0;
        }

        private void TextArea_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back)
                spaceCnt--;
        }

        private void editor_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        public void fold()
        {
            if (own.m_settings.foldingEnabled)
            { 
                foldingStrategy.UpdateFoldings(foldingManager, editor.Document);
            }
        }

        public void format()
        {
            if (own.m_settings.formatEnabled)
                if (docO.hlMode != 4 && docO.hlMode != 0)
                    indent.Indent(new TextDocumentAccessor(editor.Document), false);
        }

        public int getColumn()
        {
            return editor.TextArea.Caret.Column;
        }

        public int getLine()
        {
            return editor.TextArea.Caret.Line;
        }

        public void initCompl()
        {
            if (!own.m_settings.completion || docO.hlMode == 0)
                return;

            completion = new CompletionWindow(editor.TextArea);
            completion.CompletionList.ListBox.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x2D, 0x2D, 0x30));
            completion.CompletionList.ListBox.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0x2D, 0x2D, 0x30));
            completion.StartOffset -= 2;
            complData = completion.CompletionList.CompletionData;
            completion.CompletionList.InsertionRequested += CompletionList_InsertionRequested;
            completion.CompletionList.SizeChanged += CompletionList_SizeChanged;
            completion.CloseWhenCaretAtBeginning = true;
           

            switch (docO.hlMode)
            {
                case 1:
                    foreach (KeyValuePair<string, string> d in CompletionResources.getDataPrScript())
                    {
                        complData.Add(new CompletionData(d.Key, d.Value));
                    }

                    break;

                case 2:
                    foreach (KeyValuePair<string, string> d in CompletionResources.getDataGLSL())
                    {
                        complData.Add(new CompletionData(d.Key, d.Value));
                    }

                    break;

                case 3:
                    foreach (KeyValuePair<string, string> d in CompletionResources.getDataHLSL())
                    {
                        complData.Add(new CompletionData(d.Key, d.Value));
                    }

                    break;
                
                default:
                    break;
            }


            if (docO.hlMode != 0 && docO.hlMode != 4)
            {
                completion.Show();

                //Determine how to show only needed vals first time
             
            }

            completion.Closed += delegate
            {
                completion = null;
            };
        }

        private void CompletionList_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            if (completion != null && completion.CompletionList != null)
            {
                if (completion.CompletionList.ActualHeight < 10)
                    completion.Close();
            }
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void editor_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            own.colCntr.Text = "Col " + getColumn().ToString();
            own.lineCntr.Text = "Ln " + getLine().ToString();
            docO.m_saved = false;
            own.stateText.Text = "Unsaved";

            docO.m_tab.Background = new SolidColorBrush(System.Windows.Media.Colors.Orange);

            if (e.Key == Key.Return)
            {
                if (own.m_settings.foldingAuto)
                    fold();

                if (own.m_settings.formatAuto)
                    format();
            }

            if (e.Key == Key.Back && spaceCnt > 0)
                spaceCnt--;
        }

        private void textEditor_TextArea_TextEntering(object sender, TextCompositionEventArgs e)
        {
        }

        private void textEditor_TextArea_TextEntered(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsWhiteSpace(e.Text[0]))
            {
                spaceCnt++;
            }
            else
            {
                spaceCnt = 0;
            }

            if (e.Text[0] == '[' || e.Text[0] == ']' || e.Text[0] == '(' || e.Text[0] == ')' || e.Text[0] == '{' || e.Text[0] == '}' || e.Text[0] == ',' || e.Text[0] == '.' || e.Text[0] == ';' || e.Text[0] == ':' || e.Text[0] == '\"' || e.Text[0] == '\'')
            {
                spaceCnt = 0;
            }

            if (spaceCnt == 2)
            {
                initCompl();
            }

        }

        private void Completion_Closed(object sender, System.EventArgs e)
        {
            spaceCnt = 0;
        }
    }
}