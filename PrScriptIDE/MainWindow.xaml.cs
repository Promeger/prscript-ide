﻿using MahApps.Metro.Controls;
using Microsoft.Win32;
using PrScriptIDE.Components;
using PrScriptIDE.Helpers;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace PrScriptIDE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public List<Document> m_docs;
        public int m_openDocs;
        public string[] cmdArgs;
        private bool initialized = false;
        public ApplicationSettings m_settings = new ApplicationSettings();
        public PreferencesWindow prefs;

        public MainWindow()
        {
            InitializeComponent();
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            this.Title = "PrScript IDE";
            this.DataContext = this;
            cmdArgs = Environment.GetCommandLineArgs();

            if (m_settings.exist())
            {
                m_settings.read();
            }
            else
            {
                m_settings.create();
            }

            m_docs = new List<Document>();

            m_openDocs = -1;
            initialized = true;

            if (cmdArgs.Length < 2)
            {
                m_openDocs++;
                Components.Document doc = new Components.Document(this);
                m_docs.Add(doc);
                MainTabs.Items.Add(m_docs[m_openDocs].m_tab);
                MainTabs.SelectedIndex = m_openDocs;
                this.hl_glsl.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 2 ? true : false);
                this.hl_hlsl.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 3 ? true : false);
                this.hl_none.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 0 ? true : false);
                this.hl_prs.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 1 ? true : false);
                this.hl_pra.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 4 ? true : false);
            }
            else
            {
                m_openDocs++;
                Components.Document doc = new Components.Document(this);
                doc.readFileAndDetermineSyntax(cmdArgs[1]);
                m_docs.Add(doc);
                MainTabs.Items.Add(m_docs[m_openDocs].m_tab);
                MainTabs.SelectedIndex = m_openDocs;
                this.hl_glsl.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 2 ? true : false);
                this.hl_hlsl.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 3 ? true : false);
                this.hl_none.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 0 ? true : false);
                this.hl_prs.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 1 ? true : false);
                this.hl_pra.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 4 ? true : false);

            }

            stateText.Text = "Ready";
        }

        private void NewCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void NewCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void OpenCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void OpenCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All supported files |*.prscript;*.prasm;*.glsl;*.hlsl;*.txt|PrScript Files (*.prscript)|*.prscript|PrASM Files (*.prasm)|*.prasm|GLSL Shaders (*.glsl)|*.glsl|HLSL Shaders (*.hlsl)|*.hlsl|Text Files (*.txt)|*.txt|All Files|*.*";

            if (dlg.ShowDialog() == true)
            {
                if (MainTabs.Items.Count == 1 && m_docs.Count == 1)
                {
                    if ("untitled" == (string)m_docs[0].m_tab.Header)
                    {
                        m_openDocs--;
                        MainTabs.Items.RemoveAt(0);
                        m_docs.RemoveAt(0);
                    }
                }

                m_openDocs++;
                Components.Document doc = new Components.Document(this);
                doc.readFileAndDetermineSyntax(dlg.FileName);
                m_docs.Add(doc);
                MainTabs.Items.Add(m_docs[m_openDocs].m_tab);
                MainTabs.SelectedIndex = m_openDocs;
            }
        }

        private void SaveCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (m_openDocs > -1 && initialized)
                e.CanExecute = m_docs[MainTabs.SelectedIndex].m_saved ? false : true;
            else
                e.CanExecute = false;
        }

        private void SaveCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (m_docs[MainTabs.SelectedIndex].m_path.Length > 0)
            {
                m_docs[MainTabs.SelectedIndex].processSave();
            }
            else
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Filter = "All supported files |*.prscript;*.prasm;*.glsl;*.hlsl;*.txt|PrScript Files (*.prscript)|*.prscript|PrASM Files (*.prasm)|*.prasm|GLSL Shaders (*.glsl)|*.glsl|HLSL Shaders (*.hlsl)|*.hlsl|Text Files (*.txt)|*.txt|All Files|*.*";

                if (dlg.ShowDialog() == true)
                {
                    m_docs[MainTabs.SelectedIndex].m_path = dlg.FileName;
                    m_docs[MainTabs.SelectedIndex].processSave();
                }
            }
        }

        private void SaveAsCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void SaveAsCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "All supported files |*.prscript;*.prasm;*.glsl;*.hlsl;*.txt|PrScript Files (*.prscript)|*.prscript|PrASM Files (*.prasm)|*.prasm|GLSL Shaders (*.glsl)|*.glsl|HLSL Shaders (*.hlsl)|*.hlsl|Text Files (*.txt)|*.txt|All Files|*.*";

            if (dlg.ShowDialog() == true)
            {
                m_docs[MainTabs.SelectedIndex].m_path = dlg.FileName;
                m_docs[MainTabs.SelectedIndex].processSave();
            }
        }

        private void PrintCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void PrintCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PrintDialog dlg = new PrintDialog();

            if (dlg.ShowDialog() == true)
            {
                FlowDocument doc = Helpers.DocumentPrinter.CreateFlowDocumentForEditor(m_docs[MainTabs.SelectedIndex].m_textBox.editor, m_docs[MainTabs.SelectedIndex].m_path, m_settings.printHighl);

                doc.PageHeight = dlg.PrintableAreaHeight;
                doc.PageWidth = dlg.PrintableAreaWidth;
                doc.ColumnWidth = doc.PageWidth;
                doc.PagePadding = new Thickness(50, 30, 50, 30);

                IDocumentPaginatorSource idocument = doc as IDocumentPaginatorSource;

                dlg.PrintDocument(idocument.DocumentPaginator, "Printing...");
            }
        }

        private void AppExitCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void AppExitCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            foreach (Document d in m_docs)
            {
                if (!d.isSaved())
                {
                    var res = MessageBox.Show(this, "Save " + d.m_path + " before exit?", "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                    if (res == MessageBoxResult.Yes)
                    {
                        if (d.m_path.Length > 0)
                        {
                            d.processSave();
                        }
                        else
                        {
                            SaveFileDialog dlg = new SaveFileDialog();
                            dlg.Filter = "All supported files |*.prscript;*.prasm;*.glsl;*.hlsl;*.txt|PrScript Files (*.prscript)|*.prscript|PrASM Files (*.prasm)|*.prasm|GLSL Shaders (*.glsl)|*.glsl|HLSL Shaders (*.hlsl)|*.hlsl|Text Files (*.txt)|*.txt|All Files|*.*";

                            if (dlg.ShowDialog() == true)
                            {
                                d.m_path = dlg.FileName;
                                d.processSave();
                            }
                        }
                        MainTabs.Items.Remove(d.m_tab);
                        m_openDocs--;
                    }
                    else if (res == MessageBoxResult.No)
                    {
                        MainTabs.Items.Remove(d.m_tab);
                        m_openDocs--;
                    }
                    else
                    {
                        return;
                    }
                }
            }

            Application.Current.Shutdown();
        }

        private void UndoCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        private void UndoCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void RedoCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        private void RedoCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void CopyCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        private void CopyCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void CutCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        private void CutCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void PasteCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        private void PasteCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void FindCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void FindCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FindReplace fr = new FindReplace(m_docs[MainTabs.SelectedIndex].m_textBox);
            fr.Show();
        }

        private void ReplaceCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ReplaceCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FindReplace fr = new FindReplace(m_docs[MainTabs.SelectedIndex].m_textBox, true);
            fr.Show();
        }

        private void FormatCodeCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void FormatCodeCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            m_docs[MainTabs.SelectedIndex].m_textBox.format();
        }

        private void ApplyFoldingCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ApplyFoldingCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            m_docs[MainTabs.SelectedIndex].m_textBox.fold();
        }

        private void HelpCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void HelpCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://url.promeger.com/prscriptidehelp");
        }

        private void CloseCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CloseCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var toRem = new List<Document>();
            if (e.OriginalSource.ToString().IndexOf("MenuItem") > 0)
            {
                if (m_docs[MainTabs.SelectedIndex].m_saved)
                {
                    MainTabs.Items.Remove(m_docs[MainTabs.SelectedIndex].m_tab);
                    toRem.Add(m_docs[m_openDocs]);
                    m_openDocs--;
                }
                else
                {
                    var res = MessageBox.Show(this, "Save before exit?", "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                    if (res == MessageBoxResult.Yes)
                    {
                        if (m_docs[MainTabs.SelectedIndex].m_path.Length > 0)
                        {
                            m_docs[MainTabs.SelectedIndex].processSave();
                        }
                        else
                        {
                            SaveFileDialog dlg = new SaveFileDialog();
                            dlg.Filter = "All supported files |*.prscript;*.prasm;*.glsl;*.hlsl;*.txt|PrScript Files (*.prscript)|*.prscript|PrASM Files (*.prasm)|*.prasm|GLSL Shaders (*.glsl)|*.glsl|HLSL Shaders (*.hlsl)|*.hlsl|Text Files (*.txt)|*.txt|All Files|*.*";

                            if (dlg.ShowDialog() == true)
                            {
                                m_docs[MainTabs.SelectedIndex].m_path = dlg.FileName;
                                m_docs[MainTabs.SelectedIndex].processSave();
                            }
                        }
                        MainTabs.Items.Remove(m_docs[MainTabs.SelectedIndex].m_tab);
                        toRem.Add(m_docs[m_openDocs]);
                        m_openDocs--;
                    }
                    else if (res == MessageBoxResult.No)
                    {
                        MainTabs.Items.Remove(m_docs[MainTabs.SelectedIndex].m_tab);
                        toRem.Add(m_docs[m_openDocs]);
                        m_openDocs--;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            else
            {
                if (e.Source.ToString().IndexOf("TabItem") > 0)
                {
                    foreach (Document d in m_docs)
                    {
                        if (d.m_tab == e.Source)
                        {
                            if (d.m_saved)
                            {
                                MainTabs.Items.Remove(d.m_tab);
                                toRem.Add(m_docs[m_openDocs]);
                                m_openDocs--;
                            }
                            else
                            {
                                var res = MessageBox.Show(this, "Save before exit?", "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                                if (res == MessageBoxResult.Yes)
                                {
                                    if (d.m_path.Length > 0)
                                    {
                                        d.processSave();
                                    }
                                    else
                                    {
                                        SaveFileDialog dlg = new SaveFileDialog();
                                        dlg.Filter = "All supported files |*.prscript;*.prasm;*.glsl;*.hlsl;*.txt|PrScript Files (*.prscript)|*.prscript|PrASM Files (*.prasm)|*.prasm|GLSL Shaders (*.glsl)|*.glsl|HLSL Shaders (*.hlsl)|*.hlsl|Text Files (*.txt)|*.txt|All Files|*.*";

                                        if (dlg.ShowDialog() == true)
                                        {
                                            d.m_path = dlg.FileName;
                                            d.processSave();
                                        }
                                    }
                                    MainTabs.Items.Remove(d.m_tab);
                                    toRem.Add(m_docs[m_openDocs]);
                                    m_openDocs--;
                                }
                                else if (res == MessageBoxResult.No)
                                {
                                    MainTabs.Items.Remove(d.m_tab);
                                    toRem.Add(m_docs[m_openDocs]);
                                    m_openDocs--;
                                }
                                else
                                {
                                    return;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (m_docs[MainTabs.SelectedIndex].m_saved)
                    {
                        MainTabs.Items.Remove(m_docs[MainTabs.SelectedIndex].m_tab);
                        toRem.Add(m_docs[m_openDocs]);
                        m_openDocs--;
                    }
                    else
                    {
                        var res = MessageBox.Show(this, "Save before exit?", "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                        if (res == MessageBoxResult.Yes)
                        {
                            if (m_docs[MainTabs.SelectedIndex].m_path.Length > 0)
                            {
                                m_docs[MainTabs.SelectedIndex].processSave();
                            }
                            else
                            {
                                SaveFileDialog dlg = new SaveFileDialog();
                                dlg.Filter = "All supported files |*.prscript;*.prasm;*.glsl;*.hlsl;*.txt|PrScript Files (*.prscript)|*.prscript|PrASM Files (*.prasm)|*.prasm|GLSL Shaders (*.glsl)|*.glsl|HLSL Shaders (*.hlsl)|*.hlsl|Text Files (*.txt)|*.txt|All Files|*.*";

                                if (dlg.ShowDialog() == true)
                                {
                                    m_docs[MainTabs.SelectedIndex].m_path = dlg.FileName;
                                    m_docs[MainTabs.SelectedIndex].processSave();
                                }
                            }
                            MainTabs.Items.Remove(m_docs[MainTabs.SelectedIndex].m_tab);
                            toRem.Add(m_docs[m_openDocs]);
                            m_openDocs--;
                        }
                        else if (res == MessageBoxResult.No)
                        {
                            MainTabs.Items.Remove(m_docs[MainTabs.SelectedIndex].m_tab);
                            toRem.Add(m_docs[m_openDocs]);
                            m_openDocs--;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }

            foreach (Document d in toRem)
            {
                m_docs.Remove(d);
            }

            toRem.Clear();
        }

        private void NewPrScriptFile_Click(object sender, RoutedEventArgs e)
        {
            m_openDocs++;
            Components.Document doc = new Components.Document(this);
            doc.setHighlighting("prscript");
            m_docs.Add(doc);
            MainTabs.Items.Add(m_docs[m_openDocs].m_tab);
            MainTabs.SelectedIndex = m_openDocs;
        }

        private void NewPrASMFile_Click(object sender, RoutedEventArgs e)
        {
            m_openDocs++;
            Components.Document doc = new Components.Document(this);
            doc.setHighlighting("prasm");
            m_docs.Add(doc);
            MainTabs.Items.Add(m_docs[m_openDocs].m_tab);
            MainTabs.SelectedIndex = m_openDocs;
        }

        private void NewGlslShader_Click(object sender, RoutedEventArgs e)
        {
            m_openDocs++;
            Components.Document doc = new Components.Document(this);
            doc.setHighlighting("glsl");
            m_docs.Add(doc);
            MainTabs.Items.Add(m_docs[m_openDocs].m_tab);
            MainTabs.SelectedIndex = m_openDocs;
        }

        private void NewHlslShader_Click(object sender, RoutedEventArgs e)
        {
            m_openDocs++;
            Components.Document doc = new Components.Document(this);
            doc.setHighlighting("hlsl");
            m_docs.Add(doc);
            MainTabs.Items.Add(m_docs[m_openDocs].m_tab);
            MainTabs.SelectedIndex = m_openDocs;
        }

        private void MainTabs_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(this);
        }

        private void SetHighlNone_Click(object sender, RoutedEventArgs e)
        {
            m_docs[MainTabs.SelectedIndex].setHighlighting("none");
        }

        private void SetHighlPrScript_Click(object sender, RoutedEventArgs e)
        {
            m_docs[MainTabs.SelectedIndex].setHighlighting("prscript");
        }

        private void SetHighlPrASM_Click(object sender, RoutedEventArgs e)
        {
            m_docs[MainTabs.SelectedIndex].setHighlighting("prasm");
        }

        private void SetHighlGLSL_Click(object sender, RoutedEventArgs e)
        {
            m_docs[MainTabs.SelectedIndex].setHighlighting("glsl");
        }

        private void SetHighlHLSL_Click(object sender, RoutedEventArgs e)
        {
            m_docs[MainTabs.SelectedIndex].setHighlighting("hlsl");
        }

        private void Preferences_Click(object sender, RoutedEventArgs e)
        {
            prefs = new PreferencesWindow(this);
            prefs.Show();
        }

        private void Reference_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://url.promeger.com/prsref");
        }

        private void Prdn_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://url.promeger.com/prdn");
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow win = new AboutWindow();
            win.Show();
        }

        private void PageSettings_Click(object sender, RoutedEventArgs e)
        {
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (Document d in m_docs)
            {
                if (!d.isSaved())
                {
                    var res = MessageBox.Show(this, "Save " + d.m_path + " before exit?", "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                    if (res == MessageBoxResult.Yes)
                    {
                        if (d.m_path.Length > 0)
                        {
                            d.processSave();
                        }
                        else
                        {
                            SaveFileDialog dlg = new SaveFileDialog();
                            dlg.Filter = "All supported files |*.prscript;*.prasm;*.glsl;*.hlsl;*.txt|PrScript Files (*.prscript)|*.prscript|PrASM Files (*.prasm)|*.prasm|GLSL Shaders (*.glsl)|*.glsl|HLSL Shaders (*.hlsl)|*.hlsl|Text Files (*.txt)|*.txt|All Files|*.*";

                            if (dlg.ShowDialog() == true)
                            {
                                d.m_path = dlg.FileName;
                                d.processSave();
                            }
                        }
                        MainTabs.Items.Remove(d.m_tab);
                        m_openDocs--;
                    }
                    else if (res == MessageBoxResult.No)
                    {
                        MainTabs.Items.Remove(d.m_tab);
                        m_openDocs--;
                    }
                    else
                    {
                        return;
                    }
                }
            }

            Application.Current.Shutdown();
        }

        private void MainTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MainTabs.SelectedIndex > -1)
            {
                this.hl_glsl.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 2 ? true : false);
                this.hl_hlsl.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 3 ? true : false);
                this.hl_none.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 0 ? true : false);
                this.hl_prs.IsChecked = (m_docs[MainTabs.SelectedIndex].hlMode == 1 ? true : false);
            }
        }

        private void MainTabs_Initialized(object sender, EventArgs e)
        {
        }
    }
}